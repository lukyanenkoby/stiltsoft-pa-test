﻿# July 5, 2018. 11:32 AM


# Website Improvement Proposals


## Part One. Small Changes


### Bugs


1\. Repair the broken link _Request Feature_ on the 404 error page.


2\. Remove the broken link _Forte Print_ in the blog post [Case Study: Project Budgeting in Atlassian JIRA](https://stiltsoft.com/blog/2016/05/case-study-project-budgeting-in-atlassian-jira).


3\. Repair or remove broken links _small donations_ and _https://mercyships.org_ in the blog post [Sponsored Development: How We Improved No Email Storm Add-on](https://stiltsoft.com/blog/2015/10/no-email-storm-2-1-0-released).
 
4\. Every page in the blog refers to the file _fonts/ecf-icons.eot_. But such file does not exist. Fix it.


5\. Pages [Add-ons](https://stiltsoft.com/addons.html), [Add-ons For Jira](https://stiltsoft.com/addons-jira.html),  [Add-ons For Confluence](https://stiltsoft.com/addons-confluence.html) refers to files _css/zoom.css_ and _scripts/jquerry.prettyPhoto.js_.  But such files do not exist. Fix it.


6\. The file _robots.txt_ refers to the [Sitemap](https://stiltsoft.com/sitemap.xml). But the sitemap does not exist. Create it. 


7\. Some pages have old layouts.  


- 404 error page 
- [Talk](https://stiltsoft.com/talk)
- [Blog](https://stiltsoft.com/blog)
- [Promotions AUG](https://stiltsoft.com/promotions-aug)


Use the same layout on all pages.


### Links


8\. Replace the link on the company’s logo [stiltsoft.com/index.html](https://stiltsoft.com/index.html) by [stiltsoft.com](https://stiltsoft.com)


It would be more beautiful. The shorter, the better. Most of modern websites link to the main page as site.com, not site.com/index.html.


9\. Remove _index.html_ from internal links. 


Replace:


- [talk/index.html](https://stiltsoft.com/talk/index.html)
- [awesome-graphs-for-bitbucket/index.html](https://stiltsoft.com/awesome-graphs-for-bitbucket/index.html)


By:


- [talk](https://stiltsoft.com/talk)
- [awesome-graphs-for-bitbucket](https://stiltsoft.com/awesome-graphs-for-bitbucket)


It would be more beautiful. The shorter, the better. Most of modern websites have links /page, not  /page/index.html.


10\. Remove _.html_ extensions from internal links.


Replace:


- [about.html](https://stiltsoft.com/about.html)
- [addons.html](https://stiltsoft.com/addons.html)
- [addons-jira.html](https://stiltsoft.com/addons-jira.html)
- [addons-confluence.html](https://stiltsoft.com/addons-confluence.html)
- [sponsored-development.html](https://stiltsoft.com/sponsored-development.html)
- [support.html](https://stiltsoft.com/support.html)


By:


- [about](https://stiltsoft.com/about)
- [addons](https://stiltsoft.com/addons)
- [addons-jira](https://stiltsoft.com/addons-jira)
- [addons-confluence](https://stiltsoft.com/addons-confluence)
- [sponsored-development](https://stiltsoft.com/sponsored-development)
- [support](https://stiltsoft.com/support)


It would be more beautiful. The shorter, the better. Most of modern websites have links /page, not /page.html.


11\. Remove the item _Home_ from the main menu.


Most of modern websites do not have a home item in their menus. The link on the logo is enough.


### Main Page


12\. Replace the sentence _We’re Verified Vendor on Atlassian Marketplace_ by _Verified Vendor on Atlassian Marketplace_.


The shorter, the better. _We’re_ - unnecessary extra words.


13\. Your website mentions 3400+ customers, but according to the Marketplace, one of your products is actively used in more than 5700 instances. Does it mean, that you have 5700+ customers? If yes, update the number of customers on the website.


The more customers, the better.


14\. Replace the words _15+ ADD-ONS ON MARKETPLACE_ by _19 APPS ON MARKETPLACE_


19 is more than 15. Exact values are better than N+. Currently Atlassian names your products as apps. You should do the same.


15\. The Timeline has few milestones. Add new product launching dates as extra milestones to the Timeline.


16\. Remove the _Follow Us_ section from the Timeline.


It is duplicated in the bottom of the page.


17\. Add recent blog posts to the main page.


You will get more blog readers and subscribers.


### Website Bottom


18\. Replace words _Our Contacts_ by _Contact Us_.


Most of websites call contact information sections not _Our Contacts_, but _Contact Us_.


19\. Change the phone number to +375 29 646 5112.


Most of companies publish phone numbers in such format.


20\. Change the address to 


Gagarina Street 49,  
Suite 2-19,  
246050 Gomel  
Belarus


This address format will be compliant with national postage rules.


21\. Change the order of the contact information to 


Gagarina Street 49,  
Suite 2-19,  
246050 Gomel  
Belarus


+375 29 646 5112


info@stiltsoft.com


Most of companies first publish addresses, then phone numbers, then emails.


22\. Remove from the contact information words _Address_, _Phone_, _Email_.


They are not necessary. Most of companies do not use them.


23\. Put a link to the contact form below the contact information.


It would be an extra way to contact you.


24\. Publish a link to the Privacy Policy.


Most of software companies have it.


25\. Publish a link to the EULA.


Most of software product companies have it.


26\. Replace the text _Copyright © 2010 - 2018 StiltSoft.com_ by _Copyright © 2010 - 2018 StiltSoft_.


The copyright owner in the company, not the website.


27\. Add links to corporate profiles in Facebook, Youtube, and Instagram to the _Follow Us_ section.


Visitors will know about your profiles in social media. You will get more subscribers.


28\. In the _Support_ section add a link to [documentation](https://docs.stiltsoft.com).


It will decrease the number of support tickets.


29\. In the _Support_ section the link _Request Feature_ leads to the discussion of Awesome Graphs for Bitbucket. Instead of it use a link to the contact form.


You will get more idea suggestions.


30\. Add in the bottom _Join our Newsletter_ form.


You will get more subscribers.


31\. Remove the text _StiltSoft - Atlassian Solution Partners. Add-ons for Jira, Confluence, Bitbucket and Bamboo_.


This text is not necessary. Most of software companies websites don’t have such texts.


### Add-ons Pages


32\. Replace the text “Need a custom feature in our add-on? Ask us!” by “Need a custom feature in our apps? We can develop it for you!”


It will be more clear.


33\. Replace the link in the abovementioned text from mailto to the page [Sponsored Development](https://stiltsoft.com/sponsored-development.html).


It will give potential customers more information.


34\. Add links _For Bitbucket_ and _For Bamboo_ below links _Most Popular_, _For Jira_, _For Confluence_. List on that pages apps for Bitbucket and Bamboo.


It may increase your sales.


35\. Rename the text _Most Popular_ to _All apps_. List all products on that page.


It may increase your sales.


36\. If you implement two previous proposals, make similar changes in the Products section on the website bottom.
 
37\. Names of some apps on the website differ from their names in Marketplace.


For example:


- Talk - Inline Comments (on the website) vs. Talk - Advanced Inline Comments (on the Marketplace).
- Quizzes (on the website) vs. Courses and Quizzes - LMS for Confluence (on the Marketplace)


Use same variants of product names everywhere.


38\. Remove info about customers from the page [Awesome Graphs for Bitbucket](https://stiltsoft.com/awesome-graphs-for-bitbucket).


You already mention customers on the homepage.


39\. If you want to leave the number of customers on that product page, update it.


According to the website, you have 1200+ customers of the product. But according to the Marketplace, you have 2600+ customers.


40\. On the page [Awesome Graphs for Bitbucket](https://stiltsoft.com/awesome-graphs-for-bitbucket) make the sentence _What Customers Say_ static, not moving with customers’ quotes.


It will be more beautiful. There is no need to move the unchanged text.
 
### About Page


41\. Remove the _Contact Us_ section.


This information is duplicated in the bottom of the page.


42\. Replace _Our History_ by _History_, _Our Team_ by _Team_, _Our Customers_ by _Customers_.


_Our_ is an unnecessary extra word. The shorter, the better.


43\. In the section _Customers_ add a sentence “N+ customers from N+ countries” like on the main page.


It will be more informative.


44\. Mention, that you participate in the Pledge 1%.


It will be a positive contribution to your reputation.


45\. When a visitor clicks on an employee photo, a short text about that employee may appear. Optionally this text may include links to his or her blog posts and a personal page on the documentation portal.


Visitors will get more information about your team.


### Blog


46\. In the comments section of blog posts replace the text _StiltSoft Blog - Atlassian Silver Solutions Partner Blog_ by _StiltSoft Blog_.


It is better to avoid using the word _blog_ twice in one sentence. The shorter, the better.


47\. In the bottom section of blog posts replace words _Recommended Articles_ by _Recommended Posts_.


You name publications in your blog as posts in other places.


### Support Page


48\. Replace _Our Support_ by _Support_.


_Our_ is an unnecessary extra word. The shorter, the better.


49\. Replace _Add-ons we support_ by _Supported Apps_.


It may be a bit more beautiful.


50\. In the section _Supported Apps_ add links to documentation of three apps:


- No Email Storm
- Favorite Pages for Confluence
- Weekly Commitment


You sell these apps, but don’t currently list them on the Support page.


## Part Two. Big Changes


51\. Hire an editor (preferably a native English speaker) to proofread all website texts. 


In many places your texts can be improved. For example, on the page [Promotions AUG](https://stiltsoft.com/promotions-aug) it is better to say _We offer a 20% discount_ instead of _We are offering_.


52\. Everywhere on the website replace the word _add-on(s)_ by _app(s)_. 


Currently Atlassian names your products as apps. You should do the same.


53\. Change the website structure, and the main menu.


Products


- For Jira
- For Confluence
- For Bitbucket
- For Bamboo


Services


- Consulting
- Custom Development
- Portfolio


Company


- About Us
- History
- Customers
- Careers


Support


- Documentation
- Get Help
- Suggest an Idea


Blog


Such structure will be more informative.


54\. Create a page for every product.


It may help to increase sales.


55\. Change the main page. 


It may include the following parts:


- Verified Vendor on Atlassian Marketplace (current part).
- Apps For Jira, Confluence, Bitbucket, and Bamboo (list of all apps with a logo and a short description).
- 3400+ (or 5700+) customers from 80+ (or more) countries. Logos of big customers wit a link to the Customers page.
- Timeline (current part).
- Recent blog posts. 


Such layout will be more informative.


P. S. July 5, 2018. 11:35 AM


It was easy to suggest website improvement proposals, to learn Git, and to use Git for Windows.